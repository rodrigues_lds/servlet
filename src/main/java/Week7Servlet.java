import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * WEEK 07 - SERVLET
 *
 * @author Eduardo Rodrigues
 * @version 1.0
 * @since 2020-10-28
 * <p>
 * Write a program that takes a response from a web page (a form where you submit data).
 * The servlet should process the request and respond with another web page or response of some kind using the data that was submitted.
 * The servlet will need to run on some server (something like Tomcat).
 */

@WebServlet("/lesson")
public class Week7Servlet extends HttpServlet {

    /**
     * This function is used for sending information to the server.
     *
     * @param request  determines the request from the server.
     * @param response determines the response to be send to the server.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

        // Get the values from the form
        String week = request.getParameter("week");
        String lesson = request.getParameter("lesson");

        try {
            // Check if the values inserted in the form to determine which page to open.
            if (week.equals("week7") && lesson.equals("servlet")) {
                response.sendRedirect("guideweek7.jsp");
            } else {
                response.getWriter().println("Lesson does not exist!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
