<%--
  Created by IntelliJ IDEA.
  User: rodri
  Date: 10/29/2020
  Time: 9:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Guide Week7</title>
</head>
<body>
<div align="left">
    <h1>Week 7 - Guide</h1>
    <p>
    <ul>
        <li>Write a program that takes a response from a web page (a form where you submit data).</li>
        <li>The servlet should process the request and respond with another web page or response of some kind using the data that was submitted. </li>
        <li>The servlet will need to run on some server (something like Tomcat).</li>
    </ul>
    </p>
</div>
</body>
</html>
